// includes
const Discord = require('discord.js');
const auth    = require('./auth.json');
const funcs   = require('./funcs.js');

// inits globals
global.bonding_file = "./db/bonding.file";
global.state = {
  msg_count: 0,
  cmd_count: 0
};
global.client = new Discord.Client();

// start
client.on('ready', () => {
  funcs.clientReady();
});

// message handling
client.on('message', message => {
  funcs.clientMessage(message);
});

// connect
client.login(auth.token);
